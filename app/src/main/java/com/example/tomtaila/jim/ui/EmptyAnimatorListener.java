package com.example.tomtaila.jim.ui;

import android.animation.Animator;

/**
 * Created by tomtaila on 5/21/16.
 */
public class EmptyAnimatorListener implements Animator.AnimatorListener {
    @Override
    public void onAnimationStart(Animator animation) {

    }

    @Override
    public void onAnimationEnd(Animator animation) {

    }

    @Override
    public void onAnimationCancel(Animator animation) {

    }

    @Override
    public void onAnimationRepeat(Animator animation) {

    }
}
