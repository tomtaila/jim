package com.example.tomtaila.jim;

/**
 * Created by tomtaila on 5/10/16.
 */
public class Constants {

    public static final String URL_FIREBASE = "https://taila-jim.firebaseio.com/";
    public static final String URL_WORKOUTS = "workouts";
    public static final String URL_EXERCISES = "exercises";

    // EXTRA KEYS
    public static final String EXTRA_WORKOUT = "workout";

}
