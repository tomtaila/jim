package com.example.tomtaila.jim.ui;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Interpolator;
import android.widget.ProgressBar;

import com.example.tomtaila.jim.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by tomtaila on 5/27/16.
 */

public class CircleProgressBar extends ProgressBar {

    public static final String BUNDLE_KEY_PROGRESS = "progress";
    public static final String BUNDLE_KEY_SUPER = "super";

    int diameter;
    RectF rectF;
    Path path;
    float progressMadeStrokeWidth;
    float progressNotMadeStrokeWidth;
    int startingAngle;
    Paint progressPaint;
    Paint nonProgressPaint;
    List<Integer> colorArray = new ArrayList<>();
    int progressColor;
    int notProgressColor;
    int progress;

    ValueAnimator animator;

    public CircleProgressBar(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public CircleProgressBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attrs, R.styleable.CircleProgressBar, 0, 0);

        try {
            progressMadeStrokeWidth = typedArray.getDimensionPixelSize(R.styleable.CircleProgressBar_progress_made_width, 0);
            progressNotMadeStrokeWidth = typedArray.getDimensionPixelSize(R.styleable.CircleProgressBar_progress_not_made_width, 0);
            progressColor = typedArray.getColor(R.styleable.CircleProgressBar_progress_made_color, ContextCompat.getColor(context,R.color.colorAccent));
            notProgressColor = typedArray.getColor(R.styleable.CircleProgressBar_progress_not_made_color, ContextCompat.getColor(context, android.R.color.darker_gray));
        } finally {
            typedArray.recycle();
        }

        progressPaint = new Paint();
        progressPaint.setColor(progressColor);
        progressPaint.setStrokeWidth(progressMadeStrokeWidth);
        progressPaint.setStyle(Paint.Style.STROKE);
        progressPaint.setAntiAlias(true);

        nonProgressPaint = new Paint(progressPaint);
        nonProgressPaint.setStrokeWidth(progressNotMadeStrokeWidth);
        nonProgressPaint.setColor(notProgressColor);
    }

    private void updateProgressPaint() {
        progressPaint.setColor(progressColor);
        progressPaint.setStrokeWidth(progressMadeStrokeWidth);
        invalidate();
    }

    private void updateNotMadeProgressPaint() {
        nonProgressPaint.setStrokeWidth(progressNotMadeStrokeWidth);
        nonProgressPaint.setColor(notProgressColor);
        invalidate();
    }

    @Override
    protected synchronized void onDraw(Canvas canvas) {
        // Create bounding rectangle
        rectF = new RectF(progressMadeStrokeWidth/2, progressMadeStrokeWidth/2, diameter - (progressMadeStrokeWidth/2), diameter - (progressMadeStrokeWidth/2));

        // Draw not progress ring
        drawArc(canvas, nonProgressPaint, notProgressColor, -90, 360);
        // create path object progress arc
        startingAngle = -90;
        drawArc(canvas, progressPaint, progressColor, startingAngle, angleForProgress());
    }

    private int angleForProgress() {
        return (int) ((3.6 * progress) + 1);
    }

    private boolean isMultiColored() {
        return !colorArray.isEmpty();
    }

    private void drawArc(Canvas canvas, Paint paint, int color, int startingAngle, int sweepingAngle) {
        path = new Path();
        paint.setColor(color);
        path.addArc(rectF, startingAngle, sweepingAngle);
        canvas.drawPath(path, paint);
    }

    @Override
    protected synchronized void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Measure Width
        int width = 0;
        switch (MeasureSpec.getMode(widthMeasureSpec)) {
            case MeasureSpec.EXACTLY: // Must be this size
                width = MeasureSpec.getSize(widthMeasureSpec);
                break;
            case MeasureSpec.AT_MOST: // Can't be larger than...
                width = MeasureSpec.getSize(widthMeasureSpec);
                break;
            case MeasureSpec.UNSPECIFIED: // Can be whatever you want
                width = MeasureSpec.getSize(widthMeasureSpec);
                break;
        }

        // Measure Height
        int height = 0;
        switch (MeasureSpec.getMode(heightMeasureSpec)) {
            case MeasureSpec.EXACTLY: // Must be this size
                height = MeasureSpec.getSize(heightMeasureSpec);
                break;
            case MeasureSpec.AT_MOST: // Can't be larger than...
                height = MeasureSpec.getSize(heightMeasureSpec);
                break;
            case MeasureSpec.UNSPECIFIED: // Can be whatever you want
                height = MeasureSpec.getSize(heightMeasureSpec);
                break;
        }

        diameter = Math.min(width, height);

        setMeasuredDimension(diameter, diameter);
    }

    public float getProgressNotMadeStrokeWidth() {
        return progressNotMadeStrokeWidth;
    }

    public void setProgressNotMadeStrokeWidth(float progressNotMadeStrokeWidth) {
        this.progressNotMadeStrokeWidth = progressNotMadeStrokeWidth;
        updateNotMadeProgressPaint();
    }

    public float getProgressMadeStrokeWidth() {
        return progressMadeStrokeWidth;
    }

    public void setProgressMadeStrokeWidth(float progressMadeStrokeWidth) {
        this.progressMadeStrokeWidth = progressMadeStrokeWidth;
        updateProgressPaint();
    }

    public int getNotProgressColor() {
        return notProgressColor;
    }

    public void setNotProgressColor(int notProgressColor) {
        this.notProgressColor = notProgressColor;
        updateNotMadeProgressPaint();
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
        updateProgressPaint();
    }

    @Override
    public synchronized void setProgress(int progress) {
        super.setProgress(progress);
        this.progress = progress;
        invalidate();
    }

    public synchronized void setProgress(final int progress, boolean animated, int duration, Interpolator interpolator) {
        if(animated) {
            animator = ValueAnimator.ofFloat(0, 1);

            // animate over the course of "duration" milliseconds
            animator.setDuration(duration);

            // use a decelerate interpolator which will slow down towards the end of animation
            animator.setInterpolator(interpolator);

            // call setProgress (without animating) to
            // update the progress and invalidate the view
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float interpolation = (float) animation.getAnimatedValue();
                    int progressAtAnimation = (int) (interpolation * progress);
                    if(progressAtAnimation > CircleProgressBar.this.progress) setProgress(progressAtAnimation, false);
                }
            });

            if (!animator.isStarted()) {
                animator.start();
            }
        } else setProgress(progress);
    }

    public synchronized void setProgress(final int progress, boolean animated) {
        setProgress(progress, animated, 500, new AccelerateDecelerateInterpolator());
    }

    @Override
    public int getProgress() {
        return progress;
    }

    @Override
    public Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();

        // save our progress
        bundle.putInt(BUNDLE_KEY_PROGRESS, progress);

        // be sure to save all other instance state that may exist
        bundle.putParcelable(BUNDLE_KEY_SUPER, super.onSaveInstanceState());

        return bundle;
    }

    @Override
    public void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;

            // restore our progress
            setProgress(bundle.getInt(BUNDLE_KEY_PROGRESS));

            // restore all other state
            state = bundle.getParcelable(BUNDLE_KEY_SUPER);
        }
        super.onRestoreInstanceState(state);
    }
}
