package com.example.tomtaila.jim.ui;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewAnimationUtils;

/**
 * Created by tomtaila on 5/21/16.
 */
public class JimAnimations {

    private static final String TAG = "JimAnimations";

    public static void enterCircleReveal(View view, Animator.AnimatorListener animatorListener) {
        // get the center for the clipping circle
        int centerX = view.getMeasuredWidth() / 2;
        int centerY = view.getMeasuredHeight() / 2;

        // get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;

        createAndStartCircularReveal(view, centerX, centerY, finalRadius, animatorListener);
    }

    public static void enterCircleReveal(View view, int centerX, int centerY, Animator.AnimatorListener animatorListener) {
        // get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight()) / 2;
        createAndStartCircularReveal(view, centerX, centerY, finalRadius, animatorListener);
    }

    public static void enterCircleReveal(View view, int centerX, int centerY) {
        enterCircleReveal(view, centerX, centerY, null);
    }

    public static void entireWindowEnterCircleReveal(View view, int centerX, int centerY, @Nullable Animator.AnimatorListener animatorListener) {
        // get the final radius for the clipping circle
        int finalRadius = Math.max(view.getWidth(), view.getHeight());
        createAndStartCircularReveal(view, centerX, centerY, finalRadius, animatorListener);
    }

    public static void entireWindowEnterCircleReveal(View view, int centerX, int centerY) {
        entireWindowEnterCircleReveal(view, centerX, centerY, null);
    }

    private static void createAndStartCircularReveal(View view, int centerX, int centerY, int finalRadius, @Nullable Animator.AnimatorListener animatorListener) {
        // create the animator for this view (the start radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, centerX, centerY, 0, finalRadius);
        if(animatorListener != null) {
            anim.addListener(animatorListener);
        }

        // make the view visible and start the animation
        view.setVisibility(View.VISIBLE);
        anim.start();
    }

    void exitReveal(final View view) {
        // get the center for the clipping circle
        int cx = view.getMeasuredWidth() / 2;
        int cy = view.getMeasuredHeight() / 2;

        // get the initial radius for the clipping circle
        int initialRadius = view.getWidth() / 2;

        // create the animation (the final radius is zero)
        Animator anim =
                ViewAnimationUtils.createCircularReveal(view, cx, cy, initialRadius, 0);

        // make the view invisible when the animation is done
        anim.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                view.setVisibility(View.INVISIBLE);
            }
        });

        // start the animation
        anim.start();
    }

}
