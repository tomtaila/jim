package com.example.tomtaila.jim.ui;

import android.transition.Transition;

/**
 * Created by tomtaila on 5/21/16.
 */
public class EmptyTransitionListener implements Transition.TransitionListener {
    @Override
    public void onTransitionStart(Transition transition) {

    }

    @Override
    public void onTransitionEnd(Transition transition) {

    }

    @Override
    public void onTransitionCancel(Transition transition) {

    }

    @Override
    public void onTransitionPause(Transition transition) {

    }

    @Override
    public void onTransitionResume(Transition transition) {

    }
}
