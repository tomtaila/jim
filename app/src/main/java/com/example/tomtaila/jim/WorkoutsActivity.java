package com.example.tomtaila.jim;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;

import com.example.tomtaila.jim.models.Workout;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

public class WorkoutsActivity extends AppCompatActivity {

    private static final String TAG = "WorkoutsActivity";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private List<Workout> workouts;
    private WorkoutAdapter adapter;
    private DatabaseReference ref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_workouts);
        ButterKnife.bind(this);

        // TODO: get workouts for current user
        // get workouts
        instantiateWorkouts();

        // Setup workout adapter
        setupRecyclerView();
    }

    private void instantiateWorkouts() {
        workouts = new ArrayList<>();
        ref = FirebaseDatabase.getInstance().getReference().child(Constants.URL_WORKOUTS);
        ref.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Workout workout = dataSnapshot.getValue(Workout.class);
                workouts.add(workout);
                adapter.notifyItemInserted(workouts.size()-1);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Workout workout = dataSnapshot.getValue(Workout.class);
                int position = workouts.indexOf(workout);
                workouts.set(position, workout);
                adapter.notifyItemChanged(position);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d(TAG, "onChildRemoved: ");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

    private void setupRecyclerView() {
        adapter = new WorkoutAdapter(workouts);
        AlphaInAnimationAdapter alphaInAnimationAdapter = new AlphaInAnimationAdapter(adapter);
        alphaInAnimationAdapter.setFirstOnly(false);
        alphaInAnimationAdapter.setInterpolator(new AccelerateDecelerateInterpolator());
        recyclerView.setAdapter(alphaInAnimationAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @OnClick(R.id.btn_add_workout)
    public void startCreateWorkoutActivity() {
        Intent intent = new Intent(this, CreateWorkoutActivity.class);
        startActivity(intent);
    }

    public void onWorkoutItemClicked(View view) {
        Intent intent = new Intent(this, DoWorkoutActivity.class);
        ActivityOptionsCompat activityOptionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, ButterKnife.findById(view, R.id.btn_play), getString(R.string.transition_name_play));
        startActivity(intent, activityOptionsCompat.toBundle());
    }
}
