package com.example.tomtaila.jim;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.tomtaila.jim.models.Exercise;
import com.example.tomtaila.jim.models.Workout;
import com.example.tomtaila.jim.ui.transitions.FabTransform;
import com.example.tomtaila.jim.ui.transitions.MorphTransform;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemSelected;
import butterknife.OnTextChanged;

public class CreateCustomExerciseActivity extends AppCompatActivity {

    @BindView(R.id.container)
    CardView container;
    @BindView(R.id.spinner_muscle_group)
    Spinner spinnerMuscleGroup;
    @BindView(R.id.et_name)
    EditText etName;
    @BindView(R.id.et_weight)
    EditText etWeight;
    @BindView(R.id.et_num_reps)
    EditText etReps;
    @BindView(R.id.et_num_sets)
    EditText etSets;

    private Workout workout;
    private Exercise exercise;
    private DatabaseReference exerciseRef;
    private boolean isDismissing = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_create_exercise);
        ButterKnife.bind(this);

        workout = getIntent().getParcelableExtra(Constants.EXTRA_WORKOUT);

        setupMuscleGroupSpinner();

        storeExercise();

        if (!FabTransform.setup(this, container)) {
            MorphTransform.setup(this, container,
                    ContextCompat.getColor(this, R.color.colorAccent),
                    getResources().getDimensionPixelSize(R.dimen.cardview_default_radius));
        }

        setupSharedElementTransitionListener();
    }

    private void setupSharedElementTransitionListener() {
        setEnterSharedElementCallback(new android.app.SharedElementCallback() {
            @Override
            public View onCreateSnapshotView(Context context, Parcelable snapshot) {
                // grab the saved fab snapshot and pass it to the below via a View
                View view = new View(context);
                final Bitmap snapshotBitmap = getSnapshot(snapshot);
                if (snapshotBitmap != null) {
                    view.setBackground(new BitmapDrawable(context.getResources(), snapshotBitmap));
                }
                return view;
            }

            @Override
            public void onSharedElementStart(List<String> sharedElementNames, List<View> sharedElements, List<View> sharedElementSnapshots) {
                // grab the fab snapshot and fade it out/in (depending on if we are entering or exiting)
                for (int i = 0; i < sharedElements.size(); i++) {
                    if (sharedElements.get(i) == container) {
                        View snapshot = sharedElementSnapshots.get(i);
                        BitmapDrawable fabSnapshot = (BitmapDrawable) snapshot.getBackground();
                        fabSnapshot.setBounds(0, 0, snapshot.getWidth(), snapshot.getHeight());
                        container.getOverlay().clear();
                        container.getOverlay().add(fabSnapshot);
                        if (!isDismissing) {
                            // fab -> login: fade out the fab snapshot
                            ObjectAnimator.ofInt(fabSnapshot, "alpha", 0).setDuration(100).start();
                        } else {
                            // login -> fab: fade in the fab snapshot toward the end of the transition
                            fabSnapshot.setAlpha(0);
                            ObjectAnimator fadeIn = ObjectAnimator.ofInt(fabSnapshot, "alpha", 255)
                                    .setDuration(150);
                            fadeIn.setStartDelay(150);
                            fadeIn.start();
                        }
                        forceSharedElementLayout();
                        break;
                    }
                }
            }
        });
    }

    private Bitmap getSnapshot(Parcelable parcel) {
        if (parcel instanceof Bitmap) {
            return (Bitmap) parcel;
        } else if (parcel instanceof Bundle) {
            Bundle bundle = (Bundle) parcel;
            // see SharedElementCallback#onCaptureSharedElementSnapshot
            return (Bitmap) bundle.getParcelable("sharedElement:snapshot:bitmap");
        }
        return null;
    }

    private void forceSharedElementLayout() {
        int widthSpec = View.MeasureSpec.makeMeasureSpec(container.getWidth(),
                View.MeasureSpec.EXACTLY);
        int heightSpec = View.MeasureSpec.makeMeasureSpec(container.getHeight(),
                View.MeasureSpec.EXACTLY);
        container.measure(widthSpec, heightSpec);
        container.layout(container.getLeft(), container.getTop(), container.getRight(), container
                .getBottom());
    }

    private void dismiss() {
        isDismissing = true;
        finishAfterTransition();
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }

    @OnClick(R.id.btn_ok)
    public void onOkClick() {
        dismiss();
    }

    private void storeExercise() {
        exercise = new Exercise();
        exercise.setName(etName.getHint().toString());
        exercise.setSets(Integer.valueOf(etSets.getHint().toString()));
        exercise.setWeight(Double.valueOf(etWeight.getHint().toString()));
        exercise.setReps(Integer.valueOf(etReps.getHint().toString()));
        exercise.setMuscleGroup(spinnerMuscleGroup.getSelectedItem().toString());

        exerciseRef = FirebaseDatabase.getInstance().getReference().child(Constants.URL_EXERCISES).child(workout.getUid()).push();
        exercise.setUid(exerciseRef.getKey());
        exerciseRef.setValue(exercise);
    }

    private void setupMuscleGroupSpinner() {
        ArrayAdapter<CharSequence> muscleGroupsAdapter = ArrayAdapter.createFromResource(this, R.array.muscle_groups, R.layout.item_chosen_spinner_muscle_group);
        muscleGroupsAdapter.setDropDownViewResource(R.layout.item_spinner_muscle_group);
        spinnerMuscleGroup.setAdapter(muscleGroupsAdapter);
    }

    private void updateExercise() {
        exerciseRef.setValue(exercise);
    }

    @OnTextChanged(R.id.et_name) void updateName() {
        exercise.setName(etName.getText().toString());
        updateExercise();
    }

    @OnTextChanged(R.id.et_num_reps) void updateReps() {
        exercise.setReps(Integer.valueOf(etReps.getText().toString()));
        updateExercise();
    }

    @OnTextChanged(R.id.et_num_sets) void updateSets() {
        exercise.setSets(Integer.valueOf(etSets.getText().toString()));
        updateExercise();
    }

    @OnTextChanged(R.id.et_weight) void updateWeight() {
        exercise.setWeight(Double.valueOf(etWeight.getText().toString()));
        updateExercise();
    }

    @OnItemSelected(R.id.spinner_muscle_group) void updateMuscleGroup() {
        exercise.setMuscleGroup(spinnerMuscleGroup.getSelectedItem().toString());
        updateExercise();
    }

}
