package com.example.tomtaila.jim.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by tomtaila on 5/5/16.
 */
public class Workout implements Parcelable {

    private String title;
    private String uid;

    public Workout() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if(o instanceof Workout) {
            Workout workoutB = (Workout) o;
            if(!uid.equals(workoutB.uid)) return false;
        } else return false;

        return true;
    }

    /** PARCELABLE IMPLEMENTATION */
    protected Workout(Parcel in) {
        title = in.readString();
        uid = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(uid);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Workout> CREATOR = new Creator<Workout>() {
        @Override
        public Workout createFromParcel(Parcel in) {
            return new Workout(in);
        }

        @Override
        public Workout[] newArray(int size) {
            return new Workout[size];
        }
    };
}
