package com.example.tomtaila.jim;

import android.app.Application;

/**
 * Created by tomtaila on 5/8/16.
 */
public class JimApplication extends Application {

    private static JimApplication instance;

    public static JimApplication getInstance() {
        if(instance == null) {
            instance = new JimApplication();
        }
        return instance;
    }

    public JimApplication() {}

    @Override
    public void onCreate() {
        super.onCreate();
    }

}
