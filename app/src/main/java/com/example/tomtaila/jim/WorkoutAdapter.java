package com.example.tomtaila.jim;

import android.databinding.DataBindingUtil;
import android.os.Parcelable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tomtaila.jim.databinding.ItemWorkoutBinding;
import com.example.tomtaila.jim.models.Workout;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by tomtaila on 5/5/16.
 */
public class WorkoutAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Workout> workouts;

    public WorkoutAdapter(List<Workout> workouts) {
        this.workouts = workouts;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Creating ItemWorkoutBinding to be passed into WorkoutViewHolder constructor
        ItemWorkoutBinding workoutBinding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.item_workout,
                parent,
                false);

        // Create WorkoutViewHolder object
        WorkoutViewHolder workoutViewHolder = new WorkoutViewHolder(workoutBinding);

        return workoutViewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        WorkoutViewHolder workoutViewHolder = (WorkoutViewHolder) holder;
        ItemWorkoutBinding workoutBinding = workoutViewHolder.binding;
        Workout workout = workouts.get(position);
        workoutBinding.setWorkout(workout);
    }

    @Override
    public int getItemCount() {
        return workouts.size();
    }

    class WorkoutViewHolder extends RecyclerView.ViewHolder {

        ItemWorkoutBinding binding;

        public WorkoutViewHolder(ItemWorkoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

    }

}
