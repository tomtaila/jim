package com.example.tomtaila.jim;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.tomtaila.jim.databinding.ItemExerciseBinding;
import com.example.tomtaila.jim.models.Exercise;

import java.util.List;

/**
 * Created by tomtaila on 5/7/16.
 */
public class ExerciseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static int TYPE_EMPTY_LIST = 0;
    private static int TYPE_EXERCISE = 1;

    private List<Exercise> exercises;

    public ExerciseAdapter(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_EMPTY_LIST) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_empty_exercise_list, parent, false);
            return new EmptyListViewHolder(view);
        } else {
            // Creating ItemWorkoutBinding to be passed into WorkoutViewHolder constructor
            ItemExerciseBinding exerciseBinding = DataBindingUtil.inflate(
                    LayoutInflater.from(parent.getContext()),
                    R.layout.item_exercise,
                    parent,
                    false);

            // Create WorkoutViewHolder object
            ExerciseViewHolder exerciseViewHolder = new ExerciseViewHolder(exerciseBinding);
            return exerciseViewHolder;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(getItemViewType(position) == TYPE_EMPTY_LIST) {

        } else {
            ExerciseViewHolder exerciseViewHolder = (ExerciseViewHolder) holder;
            Exercise exercise = exercises.get(position);
            exerciseViewHolder.binding.setExercise(exercise);
        }
    }

    @Override
    public int getItemCount() {
        if(exercises.isEmpty()) return 1;
        else return exercises.size();
    }

    @Override
    public int getItemViewType(int position) {
        if(exercises.isEmpty()) return TYPE_EMPTY_LIST;
        else return TYPE_EXERCISE;
    }

    public class ExerciseViewHolder extends RecyclerView.ViewHolder {

        ItemExerciseBinding binding;

        public ExerciseViewHolder(ItemExerciseBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }

    public class EmptyListViewHolder extends RecyclerView.ViewHolder {
        public EmptyListViewHolder(View itemView) {
            super(itemView);
        }
    }
}
