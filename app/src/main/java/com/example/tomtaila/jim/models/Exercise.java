package com.example.tomtaila.jim.models;

/**
 * Created by tomtaila on 5/7/16.
 */
public class Exercise {

    private String uid;
    private String name;
    private String muscleGroup;
    private double weight;
    private int reps;
    private int sets;

    public Exercise() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMuscleGroup() {
        return muscleGroup;
    }

    public void setMuscleGroup(String muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public int getReps() {
        return reps;
    }

    public void setReps(int reps) {
        this.reps = reps;
    }

    public int getSets() {
        return sets;
    }

    public void setSets(int sets) {
        this.sets = sets;
    }

    @Override
    public String toString() {
        return "Exercise{" +
                "name='" + name + '\'' +
                ", muscleGroup='" + muscleGroup + '\'' +
                ", weight=" + weight +
                ", reps=" + reps +
                ", sets=" + sets +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if(o == null) return false;
        if(o instanceof Exercise) {
            Exercise exerciseB = (Exercise) o;
            if(!uid.equals(exerciseB.uid)) return false;
        } else return false;

        return true;
    }
}
