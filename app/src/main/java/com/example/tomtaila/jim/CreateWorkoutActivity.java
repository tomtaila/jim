package com.example.tomtaila.jim;

import android.app.ActivityOptions;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.EditText;

import com.example.tomtaila.jim.models.Exercise;
import com.example.tomtaila.jim.models.Workout;
import com.example.tomtaila.jim.ui.transitions.FabTransform;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class CreateWorkoutActivity extends AppCompatActivity {

    private static final String TAG = "CreateWorkoutActivity";

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.et_title)
    EditText etTitle;
    @BindView(R.id.btn_add_exercise)
    FloatingActionButton fab;

    private List<Exercise> exercises;
    private ExerciseAdapter adapter;
    private DatabaseReference workoutRef;
    private Workout workout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_workout);
        ButterKnife.bind(this);

        // TODO: get exercise data
        exercises = new ArrayList<>();

        setupRecyclerView();

        // store workout to firebase
        storeWorkout();

        getExercisesFromFirebase();
    }

    private void storeWorkout() {
        workout = new Workout();
        workout.setTitle(etTitle.getHint().toString());

        workoutRef = FirebaseDatabase.getInstance().getReference().child(Constants.URL_WORKOUTS).push();
        workout.setUid(workoutRef.getKey());
        workoutRef.setValue(workout);
    }

    private void updateWorkout() {
        workoutRef.setValue(workout);
    }

    // setup recycler view
    private void setupRecyclerView() {
        adapter = new ExerciseAdapter(exercises);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
    }

    @OnClick(R.id.btn_add_exercise)
    public void startCreateCustomExerciseActivity() {
        final Intent intent = new Intent(this, CreateCustomExerciseActivity.class);
        intent.putExtra(Constants.EXTRA_WORKOUT, workout);
        FabTransform.addExtras(intent, ContextCompat.getColor(this, R.color.colorAccent), R.drawable.ic_add_white_24px);
        ActivityOptions options = ActivityOptions.makeSceneTransitionAnimation(this, fab, getString(R.string.transition_name_create_custom_exercise));
        startActivity(intent, options.toBundle());
    }

    @OnTextChanged(R.id.et_title)
    public void onTitleTextChanged() {
        workout.setTitle(etTitle.getText().toString());
        updateWorkout();
    }

    public void getExercisesFromFirebase() {
        DatabaseReference exercisesRef = FirebaseDatabase.getInstance().getReference().child(Constants.URL_EXERCISES).child(workout.getUid());
        exercisesRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Exercise exercise = dataSnapshot.getValue(Exercise.class);
                exercises.add(exercise);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Exercise exercise = dataSnapshot.getValue(Exercise.class);
                int position = exercises.indexOf(exercise);
                exercises.set(position, exercise);
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
    }

}
