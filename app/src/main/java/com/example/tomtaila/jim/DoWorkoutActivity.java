package com.example.tomtaila.jim;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.LinearInterpolator;

import com.example.tomtaila.jim.ui.CircleProgressBar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by tomtaila on 5/17/16.
 */
public class DoWorkoutActivity extends AppCompatActivity {

    private static final int MILLIS_MINUTE = 60 * 1000;

    @BindView(R.id.pb_workout)
    CircleProgressBar pbWorkout;
    @BindView(R.id.pb_rest)
    CircleProgressBar pbRest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_do_workout);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.set_completed) void setCompleted(View view) {
        pbWorkout.setProgress(pbWorkout.getProgress()+15, true);
        pbRest.setProgress(100, true, MILLIS_MINUTE, new LinearInterpolator());
    }

    @OnClick(R.id.rest) void resetRest(View view) {
        pbRest.setProgress(0, false);
    }

}
